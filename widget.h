#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtNetworkAuth>
#include <QOAuth2AuthorizationCodeFlow>
#include "resources.h"
#include <QListWidgetItem>
#include <QTableWidgetItem>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

    void getTracks_Playlist();

private slots:
    void granted();

    void on_btnPlay_clicked();

    void on_btnCreatePlaylist_clicked();

    void on_btnInfo_clicked();

    void on_btnPlaylists_clicked();

    void on_btnSearch_clicked();

    void on_btnLogin_clicked();

    void on_listPlaylists_itemDoubleClicked(QListWidgetItem *item);

    void on_tableSearchTracks_itemDoubleClicked(QTableWidgetItem *item);

private:
    Ui::Widget *ui;
    QOAuthHttpServerReplyHandler * replyHandler = nullptr;
    Resources * resources;
    QOAuth2AuthorizationCodeFlow spotify;
    QDialog * dialog = nullptr;

};

#endif // WIDGET_H
