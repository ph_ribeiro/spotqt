# SPOTCUTE PROJECT
___

Author: Raphael Ribeiro

## 1. Description

This project is a challenge project. Created for QT project practice using C ++ and version control with Git.

The project consists of integrating a C ++ client with the spotify API and thus performing requests and displaying the results in a desktop program.


## 2. Motivation

Learn how to work with QT using your libraries and standards.


## 3. Technologies used

* C++
* QT
* Git
* Bitbucket
* Antergos
* GCC


## 4. Spotify API

This topic demonstrates the endpoints used by the project that have been removed from the website [Documentation about Spotify API](https://developer.spotify.com/documentation/web-api/)

* Playlist endpoints

1. Get a Playlist
"https://api.spotify.com/v1/users/{id_user}/playlists"

2. Get Tracks from a playlist
"https://api.spotify.com/v1/playlists/{id_playlist}/tracks"

3. Get search
"https://api.spotify.com/v1/search"

4. Get user informations
"https://api.spotify.com/v1/me"

5. Post create playlist
"https://api.spotify.com/v1/users/{id_user}/playlists"

6. Post insert track in a playlist
"https://api.spotify.com/v1/playlists/{playlist_id}/tracks"

7. Put play
"https://api.spotify.com/v1/me/player/play"

8. "https://api.spotify.com/v1/playlists/{playlist_id}/tracks"
