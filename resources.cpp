#include "resources.h"
#include <QtCore>
#include <QDebug>
#include <QtWidgets>
#include <QtNetworkAuth>

/* ============================================================================================= */
Resources::Resources(QString clientID, QString clientSecret, QOAuthHttpServerReplyHandler *replyHandler)
{
    this->authCodeFlow.setReplyHandler(replyHandler);
    this->authCodeFlow.setAuthorizationUrl(QUrl("https://accounts.spotify.com/authorize"));
    this->authCodeFlow.setAccessTokenUrl(QUrl("https://accounts.spotify.com/api/token"));
    this->authCodeFlow.setClientIdentifier(clientID);
    this->authCodeFlow.setClientIdentifierSharedKey(clientSecret);
    this->authCodeFlow.setScope("user-modify-playback-state user-read-private user-top-read "
                                "playlist-read-private playlist-modify-public playlist-modify-private playlist-read-collaborative");
    this->authCodeFlow.setContentType(QAbstractOAuth2::ContentType::Json);

}


/* ============================================================================================= */
Resources::Resources(){
}


/* ============================================================================================= */
Resources::~Resources(){
}


/* ============================================================================================= */
void Resources::setHandler(QOAuthHttpServerReplyHandler *replyHandler){
    this->authCodeFlow.setReplyHandler(replyHandler);
}


/* ============================================================================================= */
void Resources::setGrantPermission(){
    this->authCodeFlow.grant();
}


/* ============================================================================================= */
QString Resources::getToken(){
    return this->authCodeFlow.token();
}


/* ============================================================================================= */
void Resources::setPort(int port){
    this->port = port;
}


/* ============================================================================================= */
int Resources::getPort(){
    return this->port;
}


/* ============================================================================================= */
void Resources::setUserName(QString userName){
    this->userName = userName;
}


/* ============================================================================================= */
QString Resources::getUserName(){
    return this->userName;
}


/* ============================================================================================= */
void Resources::setPlaylist_id(QString id){
    this->playlist_id = id;
}


/* ============================================================================================= */
QString Resources::getPlaylist_id(){
    return this->playlist_id;
}


/* ============================================================================================= */
QJsonArray Resources::getPlaylists(){

    QJsonArray array = QJsonArray();

    if (this->getUserName().length() <= 0){
        return array;
    }

    QUrl uri("https://api.spotify.com/v1/users/" + this->getUserName() + "/playlists");
    auto reply = this->authCodeFlow.get(uri);

    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    const auto data = reply->readAll();
    const QJsonDocument document = QJsonDocument::fromJson(data);
    QJsonObject object = document.object();
    QJsonValue value = object.value("items");
    array = value.toArray();

    reply->deleteLater();

    return array;
}


/* ============================================================================================= */
QJsonArray Resources::getTracks(){

    QJsonArray array = QJsonArray();

    if (this->getUserName().length() <= 0 || this->getPlaylist_id().length() <= 0){
        return array;
    }

    QUrl uri("https://api.spotify.com/v1/playlists/" + this->getPlaylist_id() + "/tracks");
    auto reply = this->authCodeFlow.get(uri);

    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    const auto data = reply->readAll();
    const QJsonDocument document = QJsonDocument::fromJson(data);
    QJsonObject object = document.object();
    QJsonValue value = object["items"];
    array = value.toArray();

    reply->deleteLater();

    return array;
}


/* ============================================================================================= */
QJsonArray Resources::getSearch(QString word)
{
    QJsonArray array = QJsonArray();

    if (this->getUserName().length() <= 0){
        return array;
    }

    QUrl uri("https://api.spotify.com/v1/search");
    uri.setQuery("q="+word+"&type=track&limit=5&market=US");

    auto reply = this->authCodeFlow.get(uri);

    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    const auto data = reply->readAll();
    QJsonDocument doc = QJsonDocument::fromJson(data);
    QJsonObject obj = doc["tracks"].toObject();
    QJsonValue val = obj.value("items");
    array = val.toArray();

    reply->deleteLater();

    return array;
}


/* ============================================================================================= */
QJsonObject Resources::getUserInfo(){

    QUrl uri("https://api.spotify.com/v1/me");

    auto reply = this->authCodeFlow.get(uri);

    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    const auto data = reply->readAll();
    const QJsonDocument document = QJsonDocument::fromJson(data);
    auto root = document.object();
    this->setUserName(root.value("id").toString());
    reply->deleteLater();

    return root;
}


/* ============================================================================================= */
void Resources::postCreatePlaylist(QString name, QString description){

    if (this->getUserName().length() <= 0){
        return;
    }

    QUrl uri("https://api.spotify.com/v1/users/" + this->getUserName() + "/playlists");

    QJsonObject json_obj;
    json_obj.insert("name", name);
    json_obj.insert("description", description);
    QJsonDocument postData(json_obj);

    auto reply = this->authCodeFlow.post(uri, postData.toJson());

    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    reply->deleteLater();
}


/* ============================================================================================= */
void Resources::postInsertTrackInPlaylist(QString track){

    if (this->getUserName().length() <= 0 || this->getPlaylist_id().length() <= 0){
        return;
    }
    QUrl uri("https://api.spotify.com/v1/playlists/" + this->getPlaylist_id() + "/tracks");
    uri.setQuery("uris=" + track);

    auto reply = this->authCodeFlow.post(uri);
    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    reply->deleteLater();
}


/* ============================================================================================= */
void Resources::putPlay(QString context){

    if (this->getUserName().length() <= 0){
        return;
    }

    QUrl uri("https://api.spotify.com/v1/me/player/play");
    QJsonObject json_obj;
    json_obj.insert("context_uri", context);
    QJsonDocument postData(json_obj);

    auto reply = this->authCodeFlow.put(uri, postData.object().toVariantMap());

    QObject::connect(reply, &QNetworkReply::finished, [=]() {
        if (reply->error() != QNetworkReply::NoError) {
            return;
        }

        reply->deleteLater();
    });
}


/* ============================================================================================= */
void Resources::deleteTrackPlaylist(QString track, qint16 pos){

    if (this->getUserName().length() <= 0 || this->getPlaylist_id().length() <= 0){
        return;
    }

    QUrl uri("https://api.spotify.com/v1/playlists/" + this->getPlaylist_id() + "/tracks");

    QJsonArray array;
    QJsonObject obj;
    QJsonObject root;

    QJsonArray child_array;
    child_array.push_back(pos);
    obj.insert("positions", child_array);
    obj.insert("uri", track);
    array.push_back(obj);

    root.insert("tracks",array);

    QJsonDocument doc(root);
    //    uri.setQuery(uri);

    auto reply = this->authCodeFlow.deleteResource(uri, doc.object().toVariantMap());

    QEventLoop loop;
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    const auto data = reply->readAll();
    reply->deleteLater();
}
