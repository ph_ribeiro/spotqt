#ifndef RESOURCES_H
#define RESOURCES_H

#include <QtNetworkAuth>
#include <QOAuth2AuthorizationCodeFlow>
#include <QJsonArray>

class Resources{
public:
    Resources();
    Resources(QString clientID, QString clientSecret, QOAuthHttpServerReplyHandler *replyHandler);
    ~Resources();

    //permissions functions
    QString getToken();
    void setGrantPermission();
    void setHandler(QOAuthHttpServerReplyHandler *replyHandler);

    //resources functions
    QJsonArray getPlaylists();
    QJsonArray getSearch(QString name);
    QJsonArray getTracks();
    QJsonObject getUserInfo();
    void postCreatePlaylist(QString name, QString description);
    void postInsertTrackInPlaylist(QString track);
    void deleteTrackPlaylist(QString track, qint16 pos);
    void putPlay(QString context);

    //getters and setters
    void setPort(int port);
    int getPort();
    void setUserName(QString userName);
    QString getUserName();
    void setPlaylist_id(QString id);
    QString getPlaylist_id();

    QOAuth2AuthorizationCodeFlow authCodeFlow;

private:

    int port;
    QString userName;
    QString playlist_id;

};

#endif // RESOURCES_H
