﻿#include "widget.h"
#include "ui_widget.h"

#include <QtWidgets>
#include <QtNetworkAuth>
#include <QtDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    const QString clientId = "853380636f3d44fda8cd921df4ae1913";
    const QString clientSecret = "8bc43d50f0b6409180c320cdd9bdbf42";

    replyHandler = new QOAuthHttpServerReplyHandler(8080, this);
    replyHandler->setCallbackPath("cb/");
    resources = new Resources(clientId, clientSecret, replyHandler);


    connect(&resources->authCodeFlow, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser,
            &QDesktopServices::openUrl);

    connect(&resources->authCodeFlow, &QOAuth2AuthorizationCodeFlow::granted,
            this, &Widget::granted);

    ui->tableSearchTracks->setHorizontalHeaderItem(0,new QTableWidgetItem("Artist"));
    ui->tableSearchTracks->setHorizontalHeaderItem(1,new QTableWidgetItem("Song"));
    ui->tableSearchTracks->horizontalHeader()->setStretchLastSection(true);
    ui->tablePlaylistTracks->setHorizontalHeaderItem(0,new QTableWidgetItem("Artist"));
    ui->tablePlaylistTracks->setHorizontalHeaderItem(1,new QTableWidgetItem("Song"));
    ui->tablePlaylistTracks->horizontalHeader()->setStretchLastSection(true);

    QPixmap p(":/images/btnPlay.png");

    ui->btnPlay->setIcon(QIcon("/home/raphael/Developer/C++/QT/SpotCute/images/btnPlay.png"));

}

Widget::~Widget()
{
    delete ui;
}


void Widget::granted ()
{
    QString token = resources->getToken();
    qInfo() << "Token: " + token;

    ui->btnInfo->setEnabled(true);
}

void Widget::on_btnInfo_clicked()
{
    resources->getUserInfo();

    ui->labelUserName->setText(resources->getUserName());

    ui->btnPlaylists->setEnabled(true);
}

void Widget::on_btnPlaylists_clicked()
{

    ui->listPlaylists->clear();
    QJsonArray playlists = resources->getPlaylists();
    foreach (const QJsonValue & v, playlists){
        QListWidgetItem *newItem = new QListWidgetItem;
        newItem->setText(v.toObject().value("name").toString());
        newItem->setToolTip(v.toObject().value("id").toString());
        ui->listPlaylists->addItem(newItem);
    }
    ui->listPlaylists->setEnabled(true);
    ui->btnCreatePlaylist->setEnabled(true);
    ui->btnPlay->setEnabled(true);

}

void Widget::on_btnLogin_clicked()
{
    resources->setGrantPermission();
}

void Widget::on_listPlaylists_itemDoubleClicked(QListWidgetItem *item)
{
    resources->setPlaylist_id(item->toolTip());
    ui->lineSearchTrack->setEnabled(true);
    ui->tableSearchTracks->setEnabled(true);
    ui->btnSearch->setEnabled(true);
     ui->labelgetSelectedPlaylist->setText(item->text());

    this->getTracks_Playlist();
}

void Widget::on_btnCreatePlaylist_clicked()
{

    dialog = new QDialog();
    dialog->setFixedWidth(300);
    dialog->setFixedHeight(200);

    QLabel * name = new QLabel();
    QLabel * description = new QLabel();
    QLineEdit * nameField = new QLineEdit();
    QLineEdit * descriptionField = new QLineEdit();

    name->setText("PlayList:");
    nameField->setMaxLength(100);
    description->setText("Descrição:");
    descriptionField->setMaxLength(100);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget (name);
    layout->addWidget (nameField);
    layout->addWidget (description);
    layout->addWidget (descriptionField);

    QPushButton * buttonCreate = new QPushButton();
    buttonCreate->setText("Criar Playlist");
    layout->addWidget(buttonCreate);

    dialog->setLayout(layout);

    dialog->show();

    connect(buttonCreate, &QPushButton::clicked, [=](){
        resources->postCreatePlaylist(nameField->text(), descriptionField->text());
        dialog->close();
        on_btnPlaylists_clicked();
    });
}

void Widget::on_btnSearch_clicked()
{
    ui->tableSearchTracks->setRowCount(0);

    QString track = ui->lineSearchTrack->text();
    QJsonArray array = resources->getSearch(track);

    foreach (const QJsonValue & v, array){

        int numRows = ui->tableSearchTracks->rowCount();
        ui->tableSearchTracks->insertRow(numRows);
        QTableWidgetItem * item = new QTableWidgetItem();
        item->setText(v.toObject().value("name").toString());
        item->setToolTip(v.toObject().value("uri").toString());
        ui->tableSearchTracks->setItem(numRows, 0, item);
        QTableWidgetItem * item2 = new QTableWidgetItem();
        item2->setText(v.toObject().value("artists").toArray().takeAt(0).toObject().value("name").toString());
        ui->tableSearchTracks->setItem(numRows, 1, item2);

    }
}

void Widget::on_tableSearchTracks_itemDoubleClicked(QTableWidgetItem *item)
{

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Add Track", "Add track in playlist?", QMessageBox::Yes|QMessageBox::No);
      if (reply == QMessageBox::Yes) {
        qDebug() << "Add track in playlist";
        resources->postInsertTrackInPlaylist(item->toolTip());
        this->getTracks_Playlist();
      } else {
        qDebug() << "Do nothing";
      }
}

void Widget::on_btnPlay_clicked()
{
    resources->putPlay("spotify:playlist:"+resources->getPlaylist_id());
}

void Widget::getTracks_Playlist()
{
    ui->tablePlaylistTracks->setRowCount(0);
    ui->tablePlaylistTracks->setEnabled(true);


    QJsonArray array = resources->getTracks();

    foreach (const QJsonValue & v, array){

        int numRows = ui->tablePlaylistTracks->rowCount();
        ui->tablePlaylistTracks->insertRow(numRows);

        QTableWidgetItem * itemArtist = new QTableWidgetItem();
        itemArtist->setText(v.toObject().value("track").toObject().value("artists").toArray().takeAt(0).toObject().value("name").toString());
        ui->tablePlaylistTracks->setItem(numRows, 0, itemArtist);

        QTableWidgetItem * itemTack = new QTableWidgetItem();
        itemTack->setText(v.toObject().value("track").toObject().value("name").toString());
        itemTack->setToolTip(v.toObject().value("track").toObject().value("uri").toString());
        ui->tablePlaylistTracks->setItem(numRows, 1, itemTack);

    }
}

